// revision: 3
package de.sciss.synth
package ugen

import UGenSource._

/** A UGen implementing a physical model of a system with dry-friction. A chaotic
  * filter.
  * 
  * The input is treated as the driving force in a physical model of a mass resting
  * on a belt, which can stick or slip relative to the belt (depending on the
  * friction between the two). The input represents the position of the belt, and
  * the mass is held approximately in place by a spring and a damper. The output is
  * the position of the mass, which in very high-friction situations will be exactly
  * the same as the input – but when the other forces can overcome the friction then
  * stick-and-slip alternations will typically occur.
  * 
  * Note that DC offset will have a qualitative effect on the system's behaviour
  * (because of the spring being stretched), so feel free to experiment with
  * adding/removing DC.
  * 
  * The model is inspired by the one considered in this research article: A. Luo
  * and B. Gegg, Dynamics of a harmonically excited oscillator with dry-friction on
  * a sinusoidally time-varying, traveling surface, International Journal of
  * Bifurcation and Chaos, 16 (2006), pp. 3539–3566.
  * 
  * To create the system studied in that paper (which analyses chaotic
  * stick-and-slip oscillations), the input should be a sinusoid added to an
  * ever-increasing ramp value. But for musical effects you can do different things.
  * 
  * ===Examples===
  * 
  * {{{
  * // filter a simple sine wave, producing a chaotic result
  * play {
  *   val sig = SinOsc.ar(660)
  *   val out = Friction.ar(sig, friction = 5.41322e-5, mass = 8.05501)
  *   Pan2.ar(out, 0, 0.1)
  * }
  * }}}
  * {{{
  * // modulate parameters by mouse. Left is original (modulated) sine wave, right is filtered
  * play {
  *   val sig = SinOsc.ar((LFPulse.kr(0.5) + LFPulse.kr(0.33)).mulAdd(220, 440).lag(0.1))
  *   val out = Friction.ar(sig, friction = MouseX.kr(0.00001, 0.03, 1), mass = MouseY.kr(0.2, 10, 1))
  *   GESeq(sig, out) * 0.1
  * }
  * }}}
  * {{{
  * // Ringz oscillators, each with separate Friction, merged to create 'rusty' klank.
  * play {
  *   // Note the way the effect changes as the sound dies away.
  *   val imp = Impulse.ar(1) * 0.1
  *   val klank = Ringz.ar(imp, Seq(800, 1071, 1153, 1723))
  *   val rusty = Friction.ar(klank, friction = 1.75584e-5, mass = 2.69789)
  *   Pan2.ar(Mix(rusty))
  * }
  * }}}
  * {{{
  * // play with DC offset and spring stiffness
  * play {
  *   val sig = SinOsc.ar(330) + MouseX.kr(0.01, 10, 1)
  *   val out = Friction.ar(sig, friction = 5.41322e-5, mass = 8.05501, spring = MouseY.kr(0,1))
  *   Pan2.ar(out * 0.1)
  * }
  * }}}
  * {{{
  * // DC offset and spring stiffness for a control signal
  * play {
  *   // Converts boring sinusoidal freq undulation into something much more interesting
  *   val sig = LFPar.kr(33) + MouseX.kr(0.01, 10, 1)
  *   val fr  = Friction.kr(sig, friction = 5.41322e-5, mass = 8.05501, spring = MouseY.kr(0,1))
  *   val out = SinOsc.ar(fr.linLin(-1, 1, 150, 500))
  *   Pan2.ar(out * 0.1)
  * }
  * }}}
  * 
  * This is a third-party UGen (MCLDUGens).
  */
object Friction extends ProductReader[Friction] {
  /** 
    */
  def kr(in: GE, friction: GE = 0.5f, spring: GE = 0.414f, damp: GE = 0.313f, mass: GE = 0.1f, beltMass: GE = 1.0f): Friction = 
    new Friction(control, in, friction, spring, damp, mass, beltMass)
  
  /** 
    */
  def ar(in: GE, friction: GE = 0.5f, spring: GE = 0.414f, damp: GE = 0.313f, mass: GE = 0.1f, beltMass: GE = 1.0f): Friction = 
    new Friction(audio, in, friction, spring, damp, mass, beltMass)
  
  def read(in: RefMapIn, key: String, arity: Int): Friction = {
    require (arity == 7)
    val _rate     = in.readMaybeRate()
    val _in       = in.readGE()
    val _friction = in.readGE()
    val _spring   = in.readGE()
    val _damp     = in.readGE()
    val _mass     = in.readGE()
    val _beltMass = in.readGE()
    new Friction(_rate, _in, _friction, _spring, _damp, _mass, _beltMass)
  }
}

/** A UGen implementing a physical model of a system with dry-friction. A chaotic
  * filter.
  * 
  * The input is treated as the driving force in a physical model of a mass resting
  * on a belt, which can stick or slip relative to the belt (depending on the
  * friction between the two). The input represents the position of the belt, and
  * the mass is held approximately in place by a spring and a damper. The output is
  * the position of the mass, which in very high-friction situations will be exactly
  * the same as the input – but when the other forces can overcome the friction then
  * stick-and-slip alternations will typically occur.
  * 
  * Note that DC offset will have a qualitative effect on the system's behaviour
  * (because of the spring being stretched), so feel free to experiment with
  * adding/removing DC.
  * 
  * The model is inspired by the one considered in this research article: A. Luo
  * and B. Gegg, Dynamics of a harmonically excited oscillator with dry-friction on
  * a sinusoidally time-varying, traveling surface, International Journal of
  * Bifurcation and Chaos, 16 (2006), pp. 3539–3566.
  * 
  * To create the system studied in that paper (which analyses chaotic
  * stick-and-slip oscillations), the input should be a sinusoid added to an
  * ever-increasing ramp value. But for musical effects you can do different things.
  * 
  * This is a third-party UGen (MCLDUGens).
  */
final case class Friction(rate: MaybeRate, in: GE, friction: GE = 0.5f, spring: GE = 0.414f, damp: GE = 0.313f, mass: GE = 0.1f, beltMass: GE = 1.0f)
  extends UGenSource.SingleOut {

  protected def makeUGens: UGenInLike = 
    unwrap(this, Vector[UGenInLike](in.expand, friction.expand, spring.expand, damp.expand, mass.expand, beltMass.expand))
  
  protected def makeUGen(_args: Vec[UGenIn]): UGenInLike = {
    val _rate = rate.getOrElse(_args(0).rate)
    val _args1 = matchRate(_args, 0, _rate)
    UGen.SingleOut(name, _rate, _args1)
  }
}

/** A UGen that measures the "crest factor" of a time-domain signal. The "crest
  * factor" is the ratio of the absolute peak to the absolute mean over a certain
  * time period. In pseudocode:
  * {{{
  * crest = (samples.abs.max) / (samples.abs.mean)
  * }}}
  * 
  * For `Pulse` waves the value will be 1, because the mean and the maximum are the
  * same. For `SinOsc` , the theoretical value is `2.sqrt` . For `Saw` it is
  * `3.sqrt` . These exact values might not occur in practice, because of
  * anti-aliasing and other sampling factors.
  * 
  * This is not to be confused with `FFTCrest` which does the same thing for
  * spectral data.
  * 
  * ===Examples===
  * 
  * {{{
  * // mouse movement varies from sine wave (left) to almost-square (right)
  * play {
  *   val sig = (SinOsc.ar(MouseY.kr(100, 1000, 1)) * MouseX.kr(1,10)).clip2(1)
  *   Crest.kr(sig, 440).poll
  *   Pan2.ar(sig * 0.1)
  * }
  * }}}
  * 
  * This is a third-party UGen (MCLDUGens).
  */
object Crest extends ProductReader[Crest] {
  /** @param in               signal to analyze, can be audio rate or control rate
    * @param length           number of samples over which to take the measurement. A
    *                         buffer of this size is created internally (so be careful
    *                         about specifying a massive number here). ''(init-time
    *                         only)''
    * @param gate             normally the statistic is calculated on every control
    *                         block cycle. If one wants it less often (e.g. to reduce
    *                         CPU usage), one can modulate this – calculation only
    *                         occurs if gate is greater than zero.
    */
  def kr(in: GE, length: GE = 400, gate: GE = 1.0f): Crest = new Crest(control, in, length, gate)
  
  def read(in: RefMapIn, key: String, arity: Int): Crest = {
    require (arity == 4)
    val _rate   = in.readRate()
    val _in     = in.readGE()
    val _length = in.readGE()
    val _gate   = in.readGE()
    new Crest(_rate, _in, _length, _gate)
  }
}

/** A UGen that measures the "crest factor" of a time-domain signal. The "crest
  * factor" is the ratio of the absolute peak to the absolute mean over a certain
  * time period. In pseudocode:
  * {{{
  * crest = (samples.abs.max) / (samples.abs.mean)
  * }}}
  * 
  * For `Pulse` waves the value will be 1, because the mean and the maximum are the
  * same. For `SinOsc` , the theoretical value is `2.sqrt` . For `Saw` it is
  * `3.sqrt` . These exact values might not occur in practice, because of
  * anti-aliasing and other sampling factors.
  * 
  * This is not to be confused with `FFTCrest` which does the same thing for
  * spectral data.
  * 
  * This is a third-party UGen (MCLDUGens).
  * 
  * @param in               signal to analyze, can be audio rate or control rate
  * @param length           number of samples over which to take the measurement. A
  *                         buffer of this size is created internally (so be careful
  *                         about specifying a massive number here). ''(init-time
  *                         only)''
  * @param gate             normally the statistic is calculated on every control
  *                         block cycle. If one wants it less often (e.g. to reduce
  *                         CPU usage), one can modulate this – calculation only
  *                         occurs if gate is greater than zero.
  */
final case class Crest(rate: Rate, in: GE, length: GE = 400, gate: GE = 1.0f) extends UGenSource.SingleOut {
  protected def makeUGens: UGenInLike = unwrap(this, Vector[UGenInLike](in.expand, length.expand, gate.expand))
  
  protected def makeUGen(_args: Vec[UGenIn]): UGenInLike = UGen.SingleOut(name, rate, _args)
}

/** A UGen implementing the Goertzel algorithm which is a way to calculate the
  * magnitude and phase of a signal's content at a single specified frequency. It is
  * the equivalent of running an FFT, and then only looking at the output
  * corresponding to a single bin. If one is only interested in a small number of
  * bins, then it is more efficient; if one is interested in the majority of bins,
  * one typically wants to do an FFT instead.
  * 
  * ===Examples===
  * 
  * {{{
  * // tracking the amplitude of a known sine wave
  * play {
  *   val freqM  = 220.0
  *   // try changing freq to a matching or non-matching frequency to what we're looking for
  *   val freq   = freqM  // or try MouseY.kr(110, 440, 1)
  *   val amp    = MouseX.kr
  *   val sig    = SinOsc.ar(freq) * amp
  *   val length = 4096
  *   val g      = Goertzel.kr(sig, length, freqM)
  *   
  *   // Calc the magnitude. We also normalize it against buffer size here.
  *   val mag = (g.real.squared + g.imag.squared).sqrt / (length / 2)
  *   amp.poll(label = "Input    amplitude")
  *   mag.poll(label = "Measured amplitude")
  *   Pan2.ar(sig * 0.1)
  * }
  * }}}
  * {{{
  * // tracking control rate input
  * play {
  *   val freqM  = 22.0
  *   // try changing freq to a matching or non-matching frequency to what we're looking for
  *   val freq   = freqM  // or try MouseY.kr(110, 440, 1)
  *   val amp    = MouseX.kr
  *   val sig    = SinOsc.kr(freq) * amp
  *   val length = 100
  *   val g      = Goertzel.kr(sig, length, freqM)
  *   
  *   // Calc the magnitude. We also normalize it against buffer size here.
  *   val mag = (g.real.squared + g.imag.squared).sqrt / (length / 2)
  *   amp.poll(label = "Input    amplitude")
  *   mag.poll(label = "Measured amplitude")
  *   ()
  * }
  * }}}
  * 
  * This is a third-party UGen (MCLDUGens).
  */
object Goertzel extends ProductReader[Goertzel] {
  /** @param length           used in the same way as an FFT buffer size: The larger
    *                         this value, the better the frequency resolution, but the
    *                         worse the time resolution.
    * @param freq             target frequency in Hertz. ''(init-time only)''
    * @param hop              same meaning as in the FFT UGen. Supply a value between
    *                         zero and one, for the amount of overlap between Goertzel
    *                         "frames". The default is a hop of 1 (meaning no overlap
    *                         between frames). If you specify 0.5, then the analysis
    *                         value will be produced twice as often; 0.25, four times
    *                         as often. ''(init-time only)''
    */
  def kr(in: GE, length: GE = 1024, freq: GE, hop: GE = 1.0f): Goertzel = 
    new Goertzel(control, in, length, freq, hop)
  
  def read(in: RefMapIn, key: String, arity: Int): Goertzel = {
    require (arity == 5)
    val _rate   = in.readRate()
    val _in     = in.readGE()
    val _length = in.readGE()
    val _freq   = in.readGE()
    val _hop    = in.readGE()
    new Goertzel(_rate, _in, _length, _freq, _hop)
  }
}

/** A UGen implementing the Goertzel algorithm which is a way to calculate the
  * magnitude and phase of a signal's content at a single specified frequency. It is
  * the equivalent of running an FFT, and then only looking at the output
  * corresponding to a single bin. If one is only interested in a small number of
  * bins, then it is more efficient; if one is interested in the majority of bins,
  * one typically wants to do an FFT instead.
  * 
  * This is a third-party UGen (MCLDUGens).
  * 
  * @param length           used in the same way as an FFT buffer size: The larger
  *                         this value, the better the frequency resolution, but the
  *                         worse the time resolution.
  * @param freq             target frequency in Hertz. ''(init-time only)''
  * @param hop              same meaning as in the FFT UGen. Supply a value between
  *                         zero and one, for the amount of overlap between Goertzel
  *                         "frames". The default is a hop of 1 (meaning no overlap
  *                         between frames). If you specify 0.5, then the analysis
  *                         value will be produced twice as often; 0.25, four times
  *                         as often. ''(init-time only)''
  */
final case class Goertzel(rate: Rate, in: GE, length: GE = 1024, freq: GE, hop: GE = 1.0f)
  extends UGenSource.MultiOut {

  protected def makeUGens: UGenInLike = 
    unwrap(this, Vector[UGenInLike](in.expand, length.expand, freq.expand, hop.expand))
  
  protected def makeUGen(_args: Vec[UGenIn]): UGenInLike = UGen.MultiOut(name, rate, Vector.fill(2)(rate), _args)
  
  def real: GE = ChannelProxy(this, 0)
  
  def imag: GE = ChannelProxy(this, 1)
}